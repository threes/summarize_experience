# RESTful 是什么？
 rest是一种架构风格，如果一个架构符合REST原则，就称它为RESTful架构。
#  URL设计
##  动词 + 宾语

RESTful 的核心思想就是，客户端发出的数据操作指令都是"动词 + 宾语 "的结构。例如：GET /persons，GET是动词，persons是宾语。响应是 “状态码 + 响应内容”

------------

动词通常就是五种 HTTP 方法，对应 CRUD 操作。
- GET：读取（Read）
- POST：新建（Create）
- PUT：更新（Update）
- PATCH：更新（Update），通常是部分更新，不常用
- DELETE：删除（Delete）
注：根据 HTTP 规范，动词一律大写。

## 注意点
### 宾语必须是动词：
宾语就是 API 的 URL，是 HTTP 动词作用的对象。它应该是名词，不能是动词。比如，/persons 这个 URL 就是正确的，而下面的 URL 不是名词，所以都是错误的。
```java
/getAllPersons
/createNewPersons
/deleteAllPersons
```
###  URL中名词单数OR复数
这没有统一的规定，但是常见的操作是读取一个集合，比如GET /persons（读取所有的人信息），这里明显应该是复数。
为了统一起见，建议都使用复数 URL，比如GET /persons/2要好于GET /person/2。
###  避免多级 URL
常见的情况是，资源需要多级分类，因此很容易写出多级的 URL，比如获取“获取某个新闻下的某个评论”。

    GET /persons/10/orders/2
这种 URL 不利于扩展，语义也不明确，往往要想一会，才能明白含义。
更好的做法是，除了第一级，其他级别都用查询字符串表达，故改动如下：

    GET /orders/2?persons=10

下面是另一个例子，查询已发布的文章。你可能会设计成下面的 URL。

    GET /persons/deleted
查询字符串的写法明显更好。

    GET /persons?deleted=true

# 状态码
## 状态码必须精确
客户端的每一次请求，服务器都必须给出回应。回应包括 HTTP 状态码和数据两部分。

HTTP 状态码就是一个三位数，分成五个类别。


        1xx：相关信息
        2xx：操作成功
        3xx：重定向
        4xx：客户端错误
        5xx：服务器错误
这五大类总共包含100多种状态码，覆盖了绝大部分可能遇到的情况。每一种状态码都有标准的（或者约定的）解释，客户端只需查看状态码，就可以判断出发生了什么情况，所以服务器应该返回尽可能精确的状态码。详情状态码参见维基百科：https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

### 2xx 状态码
202 Accepted状态码表示服务器已经收到请求，但还未进行处理，会在未来再处理，通常用于异步操作。下面是一个例子：


    HTTP/1.1 202 Accepted
    
    {
      "task": {
        "href": "/api/company/job-management/jobs/2130040",
        "id": "2130040"
      }
    }
### 3XX 状态码
API 用不到301状态码（永久重定向）和302状态码（暂时重定向，307也是这个含义），因为它们可以由应用级别返回，浏览器会直接跳转，API 级别可以不考虑这两种情况。
API 用到的3xx状态码，主要是303 See Other，表示参考另一个 URL。它与302和307的含义一样，也是"暂时重定向"，区别在于302和307用于GET请求，而303用于POST、PUT和DELETE请求。收到303以后，浏览器不会自动跳转，而会让用户自己决定下一步怎么办。
### 4xx 状态码
4xx状态码表示客户端错误，主要有下面几种。

    400 Bad Request：服务器不理解客户端的请求，未做任何处理。
    401 Unauthorized：用户未提供身份验证凭据，或者没有通过身份验证。
    403 Forbidden：用户通过了身份验证，但是不具有访问资源所需的权限。
    404 Not Found：所请求的资源不存在，或不可用。
    405 Method Not Allowed：用户已经通过身份验证，但是所用的 HTTP 方法不在他的权限之内。
    410 Gone：所请求的资源已从这个地址转移，不再可用。
    415 Unsupported Media Type：客户端要求的返回格式不支持。比如，API 只能返回 JSON 格式，但是客户端要求返回 XML 格式。
    422 Unprocessable Entity ：客户端上传的附件无法处理，导致请求失败。
    429 Too Many Requests：客户端的请求次数超过限额。

### 5xx 状态码

5xx状态码表示服务端错误。一般来说，API 不会向用户透露服务器的详细信息，所以只要两个状态码就够了。

    500 Internal Server Error：客户端请求有效，服务器处理时发生了意外。
    503 Service Unavailable：服务器无法处理请求，一般用于网站维护状态

# 服务器回应

## 不要返回纯本文
API 返回的数据格式，不应该是纯文本，而应该是一个 JSON 对象，因为这样才能返回标准的结构化数据。所以，服务器回应的 HTTP 头的Content-Type属性要设为application/json。
客户端请求时，也要明确告诉服务器，可以接受 JSON 格式，即请求的 HTTP 头的ACCEPT属性也要设成application/json。下面是一个例子。

    GET /persons/2 HTTP/1.1 
    Accept: application/json

## 发生错误时，不要返回 200 状态码

有一种不恰当的做法是，即使发生错误，也返回200状态码，把错误信息放在数据体里面，就像下面这样。

    HTTP/1.1 200 OK
    Content-Type: application/json
    
    {
	  "code":500,
      "status": "failure",
      "data": {
        "error": "Expected at least two items in list."
      }
    }
	
正确做法应该是：


    HTTP/1.1 400 Bad Request
    Content-Type: application/json
    
    {
      "error": "Invalid payoad.",
      "detail": {
         "surname": "This field is required."
      }
    }


